package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Simple Icy plugin library for the BridJ library
 * 
 * @author Stephane
 */
public class BridJLibraryPlugin extends Plugin implements PluginLibrary
{
    // just to encapsulate the BridJ library
}
